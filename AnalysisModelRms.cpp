#include <TDirectory.h>
#include <TFile.h>
#include <TGraph.h>

#include "AnalysisModelRms.h"
#include "SimulationParameters.hpp"

namespace corryvreckan {
    AnalysisModelRms::AnalysisModelRms(Configuration& config, std::vector<std::shared_ptr<Detector>> detectors)
        : Module(config, detectors), mNumberOfEvents(0) {
        mInFileName = config.getPath("infile");
        mXmlFileName = config.getPath("xml_file");
        mPhantomPosition = config.get<double>("phantom_position");
        mNumberOfPlanes = config.get<std::size_t>("number_of_planes");
    }

    void AnalysisModelRms::initialize() {
        // find the particle name, to obtain rest energy
        const auto parameters = Path::XMLParameters(mXmlFileName);
        mMlpModel.reset(new MostLikelyPath(
            new Geant4KinematicTerm(mInFileName, parameters.mBeamParameters.RestEnergy(), mNumberOfPlanes),
            parameters.mBeamParameters.IonCharge()));
        mQuadraticSum.resize(mNumberOfPlanes);
    }

    StatusCode AnalysisModelRms::run(const std::shared_ptr<Clipboard>& clipboard) {
        auto differenceSquared = [](Eigen::Vector2d const& a, Eigen::Vector2d const& b) {
            auto diff = a - b;
            return (diff.x() * diff.x()) + (diff.y() * diff.y());
        };

        auto convertDirection = [](XYZVector const& p) { return Eigen::Vector2d(p.X() / p.Z(), p.Y() / p.Z()); };
        auto convertPosition = [](XYZPoint const& p) { return Eigen::Vector3d(p.X(), p.Y(), p.Z()); };
        auto dropZ = [](Eigen::Vector3d const& p) { return Eigen::Vector2d(p.x(), p.y()); };

        const auto tracks = clipboard->getData<Track>();
        const auto paths = clipboard->getData<PhantomPath>();

        if(!paths.empty()) {
            const auto path = paths.front();
            for(const auto& track : tracks) {
                const auto z0 = mPhantomPosition + path->Z().at(0);
                const auto inPosition = track->getIntercept(z0);
                const auto inDirection = track->getDirection(z0);

                const auto zN = mPhantomPosition + path->Z().at(mNumberOfPlanes - 1);
                const auto outPosition = track->getIntercept(zN);
                const auto outDirection = track->getDirection(zN);

                {
                    const auto frontPosition = Eigen::Vector3d(path->X().at(0), path->Y().at(0), z0);
                    mQuadraticSum[0] += differenceSquared(dropZ(convertPosition(inPosition)), dropZ(frontPosition));
                }
                for(std::size_t i = 1; i < mNumberOfPlanes - 1; i++) {
                    const auto z = mPhantomPosition + path->Z().at(i);
                    const auto xy = mMlpModel->ProjectXY({convertPosition(inPosition),
                                                          convertPosition(outPosition),
                                                          convertDirection(inDirection),
                                                          convertDirection(outDirection)},
                                                         z);
                    const auto zPosition = Eigen::Vector3d(path->X().at(i), path->Y().at(i), z);
                    mQuadraticSum[i] += differenceSquared(dropZ(zPosition), xy);
                }
                {
                    const auto backPosition =
                        Eigen::Vector3d(path->X().at(mNumberOfPlanes - 1), path->Y().at(mNumberOfPlanes - 1), zN);
                    mQuadraticSum[mNumberOfPlanes - 1] +=
                        differenceSquared(dropZ(convertPosition(outPosition)), dropZ(backPosition));
                }
                if(mNumberOfEvents == 0) {
                    mZ = path->Z();
                }
                ++mNumberOfEvents;
            }
        }
        return StatusCode::NoData;
    }

    void AnalysisModelRms::finalize(const std::shared_ptr<ReadonlyClipboard>&) {
        TTree outTree;
        Double_t z;
        Double_t rms;
        outTree.Branch("z", &z);
        outTree.Branch("rms", &rms);
        std::vector<double> RMS(mNumberOfPlanes, 0.);
        for(std::size_t i = 0; i < mNumberOfPlanes; i++) {
            z = mZ.at(i);
            rms = std::sqrt(mQuadraticSum.at(i) / static_cast<double>(2 * mNumberOfEvents));
            RMS[i] = rms;
            outTree.Fill();
        }
        TGraph graph(static_cast<Int_t>(mNumberOfPlanes), mZ.data(), RMS.data());

        getROOTDirectory()->WriteTObject(&outTree, "tree");
        getROOTDirectory()->WriteTObject(&graph, "graph");
    }
} // namespace corryvreckan
