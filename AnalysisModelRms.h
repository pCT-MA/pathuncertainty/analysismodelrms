#pragma once

#include "core/module/Module.hpp"
#include "objects/MostLikelyPath.hpp"
#include "objects/PhantomPath.hpp"

namespace corryvreckan {
    class AnalysisModelRms : public Module {
    public:
        AnalysisModelRms(Configuration& config, std::vector<std::shared_ptr<Detector>> detectors);

        void initialize() override;
        StatusCode run(const std::shared_ptr<Clipboard>& clipboard) override;
        void finalize(const std::shared_ptr<ReadonlyClipboard>& clipboard) override;

    private:
        static constexpr const char* gGarametersTreeName = "parameters";
        static constexpr const char* gBeamParticleBranchName = "beamParticle";

        std::unique_ptr<MostLikelyPath> mMlpModel;
        std::string mInFileName;
        std::string mXmlFileName;
        double mPhantomPosition;

        // temporary for the rms statistic
        std::size_t mNumberOfEvents;
        std::size_t mNumberOfPlanes;
        std::vector<double> mZ;
        std::vector<double> mQuadraticSum;
    };
} // namespace corryvreckan
